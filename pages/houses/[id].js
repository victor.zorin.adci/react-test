import React from 'react';

import { server } from '../../config';

import { Header } from '../../components/Header/';
import { Footer } from '../../components/Footer/';
import { SliderDetail } from '../../components/SliderDetail/';
import { Expert } from '../../components/Expert/';
import { FollowPrice } from '../../components/FollowPrice/';
import { Review } from '../../components/Review/';
import { Button } from '../../components/Button/';
import { Map } from '../../components/Map/';

import useInput from '../../hooks/InputHook';

export const HouseDetail = ({ listing }) => {
  const phone = useInput('', { isEmpty: true, minLenght: 3, isPhone: true });

  return (
    <div className="container">
      <Header />

      <main>
        <section className="search">
          <h1>Квартиры в Москве </h1>

          <div className="content">
            <div className="content__main content__main--listing-detail">
              <div className="house-sub-title">Продажа квартир и домов</div>

              <div className="house-title">
                {listing.address.state}, {listing.address.city}, {listing.address.street}
              </div>

              <div className="house-title-desc">{listing.square} Общая площадь.</div>

              <SliderDetail images={listing.images} />

              <div className="listing-param">
                <div className="listing-param__desc">{listing.description}</div>

                <div className="listing-param__info">
                  <div className="listing-param__data-block">
                    <div className="listing-param__title">Param house:</div>

                    <div className="listing-param__data">
                      <ul className="listing-param__data-list">
                        <li className="listing-param__data-list-item">
                          Builder: <span className="listing-param__data-list-item-value">{listing.builder}</span>
                        </li>
                        <li className="listing-param__data-list-item">
                          Price: <span className="listing-param__data-list-item-value">{listing.price} $</span>
                        </li>
                        <li className="listing-param__data-list-item">
                          Square: <span className="listing-param__data-list-item-value">{listing.square}</span>
                        </li>
                        <li className="listing-param__data-list-item">
                          Basement:
                          {listing.hasBasement ? (
                            <span className="listing-param__data-list-item-value">Yes</span>
                          ) : (
                            <span className="listing-param__data-list-item-value">No</span>
                          )}
                        </li>
                        <li className="listing-param__data-list-item">
                          Garage: <span className="listing-param__data-list-item-value">{listing.garage}</span>
                        </li>
                        <li className="listing-param__data-list-item">
                          Bedrooms: <span className="listing-param__data-list-item-value">{listing.bedrooms}</span>
                        </li>
                        <li className="listing-param__data-list-item">
                          Type: <span className="listing-param__data-list-item-value">{listing.type}</span>
                        </li>
                      </ul>

                      <div className="listing-param__link-block">
                        <a className="listing-param__link" href="/">
                          Хотите продать похожую квартиру?
                        </a>
                      </div>
                    </div>

                    <div className="listing-param__location">
                      <div className="listing-param__location-title">Расположение</div>

                      <div className="listing-param__location-data">
                        {listing.address.state}, {listing.address.city}, {listing.address.street}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="listing-map">
                <div className="listing-map__title">Карта</div>

                <Map className="listing-map__map" />
              </div>
            </div>

            <div className="content__contacts">
              <div className="house-price">
                <div className="house-price__total">{listing.price} $</div>
                <div className="house-price__square">
                  {Math.ceil(listing.price / listing.square)} $ - за еденицу площади.
                </div>
                <div className="house-price__credit">Возможна ипотека</div>
              </div>

              <div>
                <Expert name={listing.product} phone={listing.phone} />
              </div>

              <div className="viewing">
                <div className="viewing__title">Запишитесь на просмотр данного предложения</div>

                <div className="viewing__message">
                  <textarea
                    className="input input--textarea"
                    type="text"
                    placeholder="Здравствуйте Денис Владиславович.
                                        Прошу вас организовать просмотр объекта 
                                        1-комнатная  квартира Московская область, 
                                        г.Ивантеевка,   ул. Толмачева, дом"
                  ></textarea>
                </div>

                {phone.isDirty && phone.isEmpty && <div className="input-error">Данное поле не может быть пустым</div>}
                {phone.isDirty && phone.minLengthError && <div className="input-error">Мало символов</div>}
                {phone.isDirty && phone.phoneError && <div className="input-error">Вы ввели не номер телефона</div>}

                <div className="viewing__phone">
                  <input
                    className="input"
                    onChange={(e) => phone.onChange(e)}
                    onBlur={(e) => phone.onBlur(e)}
                    value={phone.value}
                    name="phone"
                    type="tel"
                    placeholder="Enter phone"
                  ></input>
                </div>

                <div className="viewing__btn">
                  <Button children={'Записаться'}></Button>
                </div>
              </div>

              <FollowPrice />

              <Review />
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </div>
  );
};

export default HouseDetail;

export async function getServerSideProps({ params }) {
  const response = await fetch(`${server}/houses.json`);
  const listings = await response.json();
  const listing = listings[params.id];

  return {
    props: { listing }, // will be passed to the page component as props
  };
}
 