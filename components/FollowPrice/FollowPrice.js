import React from 'react';

import useInput from '../../hooks/InputHook';

import { Button } from '../Button/';

import './styles.scss';

export const FollowPrice = () => {
  const mail = useInput('', { isEmpty: true, minLenght: 3, isEmail: true });
  return (
    <div className="follow-price">
      <div className="follow-price__title">Следить за ценой</div>

      <div className="follow-price__desc">Сообщите мне, если цена на данное предложение изменится</div>

      {mail.isDirty && mail.isEmpty && <div className="input-error">Данное поле не может быть пустым</div>}
      {mail.isDirty && mail.minLengthError && <div className="input-error">Мало символов</div>}
      {mail.isDirty && mail.emailError && <div className="input-error">Вы ввели не email</div>}

      <div className="follow-price__mail">
        <input
          className="input"
          onChange={(e) => mail.onChange(e)}
          onBlur={(e) => mail.onBlur(e)}
          value={mail.value}
          name="phone"
          type="email"
          placeholder="Enter email"
        ></input>
      </div>

      <div className="follow-price__btn">
        <Button className={'btn--blue'} children={'Начать отслеживать'}></Button>
      </div>
    </div>
  );
};

export default FollowPrice;
