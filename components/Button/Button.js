import React from 'react';
import classNames from 'classnames';

import './styles.scss';

export const Button = ({ children, onClick, className, disabled, active }) => {
  const classes = classNames('btn', className);

  return (
    <button className={classes} disabled={disabled} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
