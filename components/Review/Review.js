import React from 'react';

import './styles.scss';

export const Review = () => {
  return (
    <div className="review">
      <div className="review__title">Отзывы о нас</div>

      <div className="review__desc">
        Спасибо Денису Владиславовичу за оперативность сделки, профессионализм и индивидуальный подход к клиенту.
      </div>

      <div className="review__client-name">Сергей Михайлов (Москва)</div>

      <div className="review__all">
        <a href="/">Посмотреть все отзывы</a>
      </div>
    </div>
  );
};

export default Review;
