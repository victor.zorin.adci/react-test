import React from 'react';

import { Card } from '../Card/';

import './styles.scss';

export const HousesBoard = ({ currentListings }) => {
  const listings = currentListings.map(({ images, address, price, square, description, id }) => (
    <Card
      key={id}
      image={images[0]}
      state={address.state}
      city={address.city}
      price={price}
      square={square}
      description={description}
      id={id}
    />
  ));

  if (listings == false) {
    return <div>Нет предложений</div>;
  }

  return <div className='house-board'>{listings}</div>;
};

export default HousesBoard;
