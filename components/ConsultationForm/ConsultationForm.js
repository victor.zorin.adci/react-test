import React from 'react';

import useInput from '../../hooks/InputHook';

import { Button } from '../Button/';

import './styles.scss';

export const ConsultationForm = () => {
  const phone = useInput('', { isEmpty: true, minLenght: 3, isPhone: true });

  return (
    <div className="consultation">
      <div className="consultation__title">Запишитесь на бесплатную консультацию в офисе</div>

      <div className="consultation__name-input">
        <input className="input" type="text" placeholder="Ваше имя"></input>
      </div>

      {phone.isDirty && phone.isEmpty && <div className="input-error">Данное поле не может быть пустым</div>}
      {phone.isDirty && phone.minLengthError && <div className="input-error">Мало символов</div>}
      {phone.isDirty && phone.phoneError && <div className="input-error">Вы ввели не номер телефона</div>}

      <div className="consultation__name-phone">
        <input
          className="input"
          onChange={(e) => phone.onChange(e)}
          onBlur={(e) => phone.onBlur(e)}
          value={phone.value}
          name="phone"
          type="tel"
          placeholder="Enter phone"
        ></input>
      </div>

      <div className="consultation__data-time">
        <input className="input" type="date"></input>
        <input className="input" type="time"></input>
      </div>

      <div className="consultation__btn">
        <Button children={'Записаться'}></Button>
      </div>
    </div>
  );
};

export default ConsultationForm;
