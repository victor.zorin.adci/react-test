import React from 'react';
import { Button } from '../Button/';

import './styles.scss';

export const Operator = () => {
  return (
    <div className="operator">
      <div className="operator__title">
        Здравствуйте, я специалист по этому району. <span className="operator__title-bold">Обращайтесь!</span>
      </div>

      <div className="operator__image">
        <img src="/svg/operator.svg" alt="operator" />
      </div>

      <div className="operator__phone">
        <a href="tel:+71234567890">+ 7 (123) 456-78-90</a>
      </div>

      <div className="operator__btn">
        <Button children={'Напишите мне'}></Button>
      </div>
    </div>
  );
};

export default Operator;
