import React from 'react';
import Link from 'next/link';

import './styles.scss';

export const Card = ({ image, state, city, price, square, description, id }) => {
  return (
    <div className="card">
      <div className="card__image">
        <img className="card__image-prev" src={image}></img>
      </div>

      <div className="card__desc">
        <div className="card__location-price">
          <div className="card__desc-location">
            <div>State: {state}</div>
            <div>City: {city}</div>
          </div>

          <div className="card__desc-price">Price: {price} $</div>
        </div>

        <div className="card__desc-square">Square: {square} M2</div>

        <div className="card__desc-text">{description.slice(0, 300)} ...</div>

        <Link href={`/houses/${id}`}>
          <div className="card__link">Подробнее >>></div>
        </Link>
      </div>
    </div>
  );
};

export default Card;
