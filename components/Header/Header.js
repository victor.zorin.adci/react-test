import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import './styles.scss';
import { FaBars, FaTimes } from 'react-icons/fa';

export const Header = () => {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <header className="header">
      <div className="header__wrapper">
        <div className="header__location-menu">
          <div className="header__location">Офис на Маяковской</div>

          <div className="header__location-adress">Москва, ст. метро «Маяковская» / ул. 2-ая Брестская, 8, этаж 10</div>

          <div className="header__location-phone">
            <a href="tel:+71234567890">+ 7 (123) 456-78-90</a>
          </div>

          <div className="header__location-vacancy">
            <Link href="/">
              <a>Вакансии</a>
            </Link>
          </div>

          <div className="header__location-search">
            <button> </button>
          </div>
        </div>
      </div>

      <div className="header__container">
        <div className="header__wrapper">
          <div className="header__nav-menu">
            <div className="nav__burger-icon" onClick={handleClick}>
              {click ? <FaTimes /> : <FaBars />}
            </div>
            <div className="header__logo">
              <Link href="/">
                <a>
                  <img src="/assets/svg/logo.svg" />
                </a>
              </Link>
            </div>

            <nav className="nav">
              <ul className={click ? 'nav__menu-list nav__menu-list--active' : 'nav__menu-list'}>
                <li className="nav__menu-list-item">
                  <Link href="/" onClick={closeMobileMenu}>
                    <a>Главная</a>
                  </Link>
                </li>

                <li className="nav__menu-list-item">
                  <Link href="/" onClick={closeMobileMenu}>
                    <a>Недвижимость</a>
                  </Link>
                </li>

                <li className="nav__menu-list-item">
                  <Link href="/" onClick={closeMobileMenu}>
                    <a>Услуги</a>
                  </Link>
                </li>

                <li className="nav__menu-list-item">
                  <Link href="/" onClick={closeMobileMenu}>
                    <a>О нас</a>
                  </Link>
                </li>

                <li className="nav__menu-list-item">
                  <Link href="/" onClick={closeMobileMenu}>
                    <a>Отзывы</a>
                  </Link>
                </li>

                <li className="nav__menu-list-item">
                  <Link href="/" onClick={closeMobileMenu}>
                    <a>Контакты</a>
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
