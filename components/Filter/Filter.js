import React from 'react';
import { Button } from '../Button/';

import './styles.scss';

export const Filter = ({
  minValue,
  maxValue,
  checkBasement,
  type,
  onFilterListings,
  setMinValue,
  setMaxValue,
  setType,
  setCheckBasement,
  onResetFilter,
}) => {
  return (
    <div className="filter">
      <div className="filter__input-group">
        <div className="filter__input-group-price">
          <div className="filter__input-price">
            <input
              className="input"
              onChange={(e) => setMinValue(e.target.value)}
              placeholder="min price"
              type="number"
            />
          </div>

          <div className="filter__input-price">
            <input
              className="input"
              onChange={(e) => setMaxValue(e.target.value)}
              placeholder="max price"
              type="number"
            />
          </div>

          <div className="filter__select-type">
            <select className="input" onChange={(e) => setType(e.target.value)}>
              <option value={''}>SingleFamily or MultiFamily</option>
              <option value="SingleFamily">SingleFamily</option>
              <option value="MultiFamily">MultiFamily</option>
            </select>
          </div>
        </div>

        <div className="filter__basement">
          <input className="check" onChange={() => setCheckBasement(!checkBasement)} type="checkbox" id="basement" />
          <label className="label" htmlFor="basement">
            Basement
          </label>
        </div>
      </div>

      <div className="filter__input-group filter__input-group--reset">
        <div className="filter__btn">
          <Button className={'btn--blue'} onClick={() => onFilterListings(minValue, maxValue, checkBasement, type)}>
            Filter
          </Button>
        </div>

        <div className="filter__btn filter__btn--reset">
          <Button className="filter__reset" onClick={() => onResetFilter()}>
            Reset filter
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Filter;
