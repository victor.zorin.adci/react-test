import React from 'react';

import { Button } from '../Button/';

import './styles.scss';

export const Expert = ({ name, phone }) => {
  return (
    <div className="expert">
      <div className="expert__name">
        Эксперт <br />
        {name}
      </div>

      <div className="expert__image">
        <img src="/svg/expert.svg" alt="expert" />
      </div>

      <div className="expert__work-time">
        Режим работы <br />
        Пн - Пт <br />с 10:00 до 19:00
      </div>

      <div className="expert__phone">
        <a href="tel:+71234567890">{phone}</a>
      </div>

      <div className="expert__btn">
        <Button children={'Напишите мне'}></Button>
      </div>
    </div>
  );
};

export default Expert;
