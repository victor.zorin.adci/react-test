import React, { useState, useRef, useEffect } from 'react';

import './styles.scss';

export const SortPopup = ({ items, activeSortType, onClickSortType }) => {
  const [visiblePopup, setVisiblePopup] = useState(false);
  const sortRef = useRef();

  const toggleVisiblePopup = () => {
    setVisiblePopup(!visiblePopup);
  };

  const handleOutsideClick = (event) => {
    const path = event.path || (event.composedPath && event.composedPath());
    if (!path.includes(sortRef.current)) {
      setVisiblePopup(false);
    }
  };

  const onSelectItem = (index) => {
    if (onClickSortType) {
      onClickSortType(index);
    }
    setVisiblePopup(false);
  };

  useEffect(() => {
    document.body.addEventListener('click', handleOutsideClick);
  }, []);

  return (
    <div ref={sortRef} className="sort">
      <div className="sort__label">
        <b>Сортировка по:</b>
        <span className="sort__active-item" onClick={toggleVisiblePopup}>
          {activeSortType}
        </span>
      </div>
      {visiblePopup && (
        <div className="sort__popup">
          <ul className="sort__popup-list">
            {items &&
              items.map((obj, index) => (
                <li
                  onClick={() => onSelectItem(obj)}
                  className={
                    activeSortType === obj.name
                      ? 'sort__popup-list-item sort__popup-list-item--active'
                      : 'sort__popup-list-item'
                  }
                  key={`${obj.type}_${index}`}
                >
                  {obj.name}
                </li>
              ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default SortPopup;
