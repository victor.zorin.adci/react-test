import React from 'react';
import Link from 'next/link';

import './styles.scss';

export const Footer = () => {
  return (
    <footer>
      <div className="footer__wrapper">
        <div className="footer__contacts">
          <nav className="footer-nav">
            <ul className="footer-nav__list">
              <li className="footer-nav__list-item">
                <Link href="/">
                  <a>Главная</a>
                </Link>
              </li>

              <li className="footer-nav__list-item">
                <Link href="/">
                  <a>Недвижимость</a>
                </Link>
              </li>

              <li className="footer-nav__list-item">
                <Link href="/">
                  <a>Услуги</a>
                </Link>
              </li>

              <li className="footer-nav__list-item">
                <Link href="/">
                  <a>О нас</a>
                </Link>
              </li>

              <li className="footer-nav__list-item">
                <Link href="/">
                  <a>Отзывы</a>
                </Link>
              </li>

              <li className="footer-nav__list-item">
                <Link href="/">
                  <a>Контакты</a>
                </Link>
              </li>
            </ul>
          </nav>

          <div className="footer__info">
            <div className="footer__info-adress">
              Адрес: <br />
              г. Москва метро «Случайное» <br />
              ул. Первая д.2 подъезд 3, этаж 4 <br />
            </div>

            <div className="footer__info-contacts">
              Контактная информация <br />
              +7 (123) 456-78-90 <br />
              +7 (123) 456-78-90 (факс) <br />
            </div>

            <div className="footer__info-time">
              Время работы: <br />
              пн-пт: 09.00 до 21.00 <br />
              сб-вс: 10.00 до 17.00 <br />
            </div>
          </div>
        </div>
      </div>

      <div className="footer__social">
        <div className="footer__social-wrapper">
          <div className="footer__social-year">2018</div>

          <div className="footer__social-icons">
            <Link href="/">
              <a>
                <div className="footer__social-icon footer__social-icon--google"></div>
              </a>
            </Link>

            <Link href="/">
              <a>
                <div className="footer__social-icon footer__social-icon--vk"></div>
              </a>
            </Link>

            <Link href="/">
              <a>
                <div className="footer__social-icon footer__social-icon--skype"></div>
              </a>
            </Link>

            <Link href="/">
              <a>
                <div className="footer__social-icon footer__social-icon--facebook"></div>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
